const card_pri = document.querySelector(".col-1 .card-container");
const card_pro = document.querySelector(".col-2 .card-container");
const card_done = document.querySelector(".col-3 .card-container");

const card_pri_heading = document.querySelector(".col-1 .col-heading");
const card_pro_heading = document.querySelector(".col-2 .col-heading");
const card_done_heading = document.querySelector(".col-3 .col-heading");
const hamburger = document.querySelector(".hamburger");

card_pri_heading.addEventListener("click", function () {
  const style_value = card_pri.style.display;
  if (style_value == "none") {
    card_pri.style.display = "block";
  } else {
   card_pri.style.display = "none";
  }
})

card_pro_heading.addEventListener("click", function () {
  const style_value = card_pro.style.display;
  if (style_value == "none") {
    card_pro.style.display = "block";
  } else {
   card_pro.style.display = "none";
  }
});

card_done_heading.addEventListener("click", function () {
  const style_value =card_done.style.display;
  if (style_value == "none") {
    card_done.style.display= "block";
  } else {
    card_done.style.display = "none";
  }
});

hamburger.addEventListener("click", function () {
  const hamburger_value = document.querySelector(".sidebar").style.display;
  if (hamburger_value == "none") {
    document.querySelector(".sidebar").style.display = "block";
  } else {
    document.querySelector(".sidebar").style.display = "none";
  }
});
